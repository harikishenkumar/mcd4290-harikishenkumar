//Q1
let pi = Math.PI;
let r = 4;
let c = (2 * pi * r);
console.log(c.toFixed(2));

//Calculate circumference
//console log
//2 decimal places


//Q2
  //substring
    //string.substring(start, end)
      //starts from 0, end is up to but doesn't include the index
let animalString = "cameldogcatlizard";
let andString = " and ";

let cat = animalString.substring(8, 11)
//expected result cat

let dog = animalString.substring(5, 8)
//expected result dog
console.log(cat+andString+dog)

  //substr 
    //string.substr(start, length)
let cat2 = animalString.substr(8,3)
let dog2 = animalString.substr(5,3)
console.log(cat2+andString+dog2)

//Q3
const person = {firstName :"Kanye",
                lastName :"West",
                birthDate :"8 June 1977",
                annualIncome :150000000.00};
console.log(person.firstName + " " + person.lastName + " was born on " + person.birthDate + " and has an annual income of $" + person.annualIncome)

//Q4
var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
//HERE your code to swap the values in number1 and number2
  //create temporary variable
let tempnumber;
  //swap variables
    //store number1 to tempnumber
tempnumber = number1;
    //assign value of number2 to number1
number1 = number2;
    //value of tempnumber is assigned to number 2
number2 = tempnumber
console.log("number1 = " + number1 + " number2 = " + number2);

//Q5
let year;
let yearNot2015Or2016;
year = 2016
  //should be && (and) not || (or)
    //&& considers both 2015 and 2016
      //|| only considers either value, hence will always return "true" if year = 2015 or year = 2016
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);